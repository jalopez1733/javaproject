// Función para validar que el mensaje devuelto por SonarQube es correcto
def qualityGateValidation(qg) {
    if (qg.status != 'OK') {
        return true
    }
    return false
}

pipeline {
    agent any

    tools {
        maven 'maven-tool'
    }

    environment {
        // General Variables for Pipeline
        PROJECT_BASE = ''
        PROJECT_ROOT = 'src' 
        EMAIL_ADDRESS = 'jalopez1733@gmail.com'
        REGISTRY = 'judinicambell/docker-termo-build'
        COMMIT_NAME_USER = ''
        COMMIT_EMAIL_USER = ''
        COMMIT_TEXT = ''
    }
    stages {
        stage('Checkout') {
            steps {
                //Obtener el repositorio de Github usando las credenciales de Github (previamente añadidas a las credenciales de Jenkins)
                checkout([$class: 'GitSCM', branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/jalopez1733/javaproject.git']]])
            }
        }
        stage('config repository git') {
            steps {
                script {
                    withCredentials([gitUsernamePassword(credentialsId: '42683580-e2c7-412c-b44c-eb9211d9f873', gitToolName: 'git-tool')]) { //accede a la  informacion del repositorio 
                        sh "git config --global user.name 'jenkins' " //configura usuario de git con comandos sh
                        sh 'git config --global user.email jenkins@gmail.com'
                        sh "git config user.name 'jenkins' "
                        sh 'git config user.email jenkins@gmail.com' 
                        COMMIT_NAME_USER = sh(  
                            script: 'git log -1 origin/main --pretty=format:%an',
                            returnStdout: true
                        ).trim()

                        COMMIT_EMAIL_USER = sh(
                            script: 'git log -1 origin/main --pretty=format:%ae',
                            returnStdout: true
                        ).trim()

                        COMMIT_TEXT = sh( //obtiene la informacion del ultimo commit
                            script: 'git log -1 origin/main --pretty=format:"%H %s"',
                            returnStdout: true
                        ).trim()

                        echo "Name-GIT ${COMMIT_NAME_USER}"
                        echo "EMAILGIT ${COMMIT_EMAIL_USER}"
                        echo "TEXT-GIT ${COMMIT_TEXT}"
                    }
                }
            }
        }
        stage('Build maven') { // este paso construye el proyecto maven
            steps {
                sh 'mvn -version'
                sh 'mvn clean'
            } 
        }
        stage('Unit test') {  // este paso realiza las pruebas unitarias creadas por los desarrolladores 
            steps {
                sh 'mvn clean compile test package'
            }
        }
        stage('sonarqube-scan') { // este paso realiza un scanner del proyecto con sonarqube validando que la estrutura se encuentre bien 
            environment {
                scannerHome = tool 'sonar-scanner'
            }
            steps {
                withSonarQubeEnv('sonarqube') {
                    sh 'mvn -version; mvn sonar:sonar \
                        -Dsonar.projectKey=javaproject:Test \
                        -Dsonar.projectName=javaproject  \
                        -Dsonar.host.url=http://mysonarqube:9000 '
                }
                timeout(time: 10, unit: 'MINUTES') {
                    //En caso de fallo de SonarQube o de exceder el tiempo de espera directo, detenga Pipeline
                    waitForQualityGate abortPipeline: qualityGateValidation(waitForQualityGate())
                }
            }
        }
        stage('push in prod') { // este paso toma todo los cambios que se realizaron y los guarda en la rama de prod si todo salio bien
            environment {
                GIT_AUTH = credentials('42683580-e2c7-412c-b44c-eb9211d9f873')
            }
            steps {
                script {
                    withCredentials([gitUsernamePassword(credentialsId: '42683580-e2c7-412c-b44c-eb9211d9f873', gitToolName: 'git-tool')]) {
                        sh "git config --global user.name 'jenkins' "
                        sh 'git config --global user.email jenkins@gmail.com'
                        sh 'git add . '
                        sh "git commit -m '[Prod]: ${COMMIT_TEXT} --build-- ${BUILD_NUMBER}' "
                        sh 'git checkout prod'
                        sh 'git merge main'
                        sh 'git push origin prod'
                    }
                }
            }
        }
    }
}

